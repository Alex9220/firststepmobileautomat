import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.URL;



public class FirstStepTest {


    @Test
    public void SignUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Pixel5");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "10");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("app", "D:/studies/appium/Android-NativeDemoApp-0.2.1.apk");

        MobileDriver driver1 = new AndroidDriver (new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        MobileElement menuLoginButton = (MobileElement) driver1.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Login\"]/android.view.ViewGroup");
        menuLoginButton.click();
        Thread.sleep(2000);
        MobileElement signUp = (MobileElement) driver1.findElementByXPath("//android.view.ViewGroup[@content-desc=\"button-sign-up-container\"]/android.view.ViewGroup/android.widget.TextView");
        signUp.click();
        Thread.sleep(2000);
        MobileElement signUpButtom = (MobileElement) driver1.findElementByAccessibilityId("button-SIGN UP");
        signUpButtom.click();
        Thread.sleep(2000);
        MobileElement failureText = (MobileElement) driver1.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView");
        Assert.assertEquals(failureText.getText(), "Some fields are not valid!");

        driver1.quit();
    }

}


